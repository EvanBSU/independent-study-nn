import numpy as np
from scipy.sparse import lil_matrix
from glob import glob
from os.path import isfile

import loadMorphCT
import calculate_angles

def plot_angle_distributions(all_angles):
    """
    Plot a simple histogram of the angle/dihedral
    distributions.

    Mostly for troubleshooting
    """
    import matplotlib.pyplot as plt
    angles = all_angles[:,:4].flatten()
    dihedrals = all_angles[:,4:].flatten()
    fig = plt.figure(figsize=(12,6))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.hist(angles)
    ax2.hist(dihedrals)
    ax1.set_xlabel("Angle")
    ax2.set_xlabel("Dihedral")
    plt.show()

def iterate_through_pairs(chromophoreList, all_angles):
    combined_angle_energy_list = []
    for i, chromophore in enumerate(chromophoreList[:500]):
        for neighbor in zip(chromophore.neighbours, chromophore.neighboursDeltaE):
            index = neighbor[0][0]
            delta_E = neighbor[1]
            self_angles = all_angles[i]
            neighbor_angles = all_angles[index]
            d_angles = neighbor_angles - self_angles
            combined = np.hstack((d_angles, np.array([delta_E])))
            combined_angle_energy_list.append(combined)
    return np.array(combined_angle_energy_list)

if __name__ == "__main__":
    directory_list = glob("/Users/evanmiller/Projects/Runs/DBP/lower_density/workspace/*/*-frame-1")
    combined_list = np.empty(0)

    if isfile("data.npy"):
        data = np.load("data.npy")
        np.random.shuffle(data)
        Ntrain = int(len(data)*0.8)
        Ntotal = len(data)
        Ntest = int((Ntotal-Ntrain)*0.75)
        Nval = int((Ntotal-Ntrain)*0.25)
        print(Ntotal, Ntrain, Ntest, Nval)
        training = data[:Ntrain]
        testing = data[Ntrain:(Ntrain+Ntest)]
        validation = data[-Nval:]
        print(training.shape, testing.shape, validation.shape)
        np.save("train", training)
        np.save("test", testing)
        np.save("validate", validation)
    else:
        for directory in directory_list:
            AAMorphologyDict, CGMorphologyDict, CGToAAIDMaster, parameterDict, chromophoreList, KMC = loadMorphCT.load_run(directory)
            SimulationBox = np.array([AAMorphologyDict['lx'], AAMorphologyDict['ly'], AAMorphologyDict['lz']])
            all_angles = calculate_angles.calculate_mol_angles(AAMorphologyDict['position'], SimulationBox)
            if len(combined_list) < 1:
                combined_list = iterate_through_pairs(chromophoreList, all_angles)
            else:
                add_list = iterate_through_pairs(chromophoreList, all_angles)
                combined_list = np.vstack((combined_list, add_list))
        np.save("data", combined_list)
