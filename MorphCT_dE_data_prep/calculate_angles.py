import numpy as np

#########################NOTE####################################
#
#
#
#  Script Assumes 500 DBP molecules located before other types
#
#
#
#################################################################

def pbc(vec, axes):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def create_filters():
    bond_indices = np.array([[46, 12],[5, 40],[58, 26],[33, 52]])
    bonds = np.array([bond_indices + 64*i for i in range(500)])
    angle_indices = np.array([[46, 12, 13],[4, 5, 40],[58, 26, 25],[34, 33, 52]])
    angles = np.array([angle_indices + 64*i for i in range(500)])
    dihedral_indices = np.array([[47, 46, 12, 13],[4, 5, 40, 41],[59, 58, 26, 25],[34, 33, 52, 53]])
    dihedrals = np.array([dihedral_indices + 64*i for i in range(500)])
    return bonds, angles, dihedrals

def calculate_mol_angles(positions, box):
    bonds, angles, dihedrals = create_filters()
    bonds = np.sort(calculate_bonds(bonds, positions, box))
    angles = np.sort(calculate_angles(angles, positions, box))
    dihedrals = np.sort(calculate_dihedrals(dihedrals, positions, box))
    combined = np.hstack((bonds,angles, dihedrals))
    return combined

def link_to_pos(array, positions):
    return np.array([[positions[atom] for atom in val] for val in array])

def calculate_bonds(indices, positions, box):
    angles = np.array([calc_bond_val(link_to_pos(mol, positions), box) for mol in indices])
    return angles

def calc_bond_val(mol, box):
    bonds = []
    for bond in mol:
        a = pbc(bond[0] - bond[1], box)
        a = np.linalg.norm(a)
        bonds.append(a)
    return np.array(bonds)

def calculate_angles(indices, positions, box):
    angles = np.array([calc_angle_val(link_to_pos(mol, positions), box) for mol in indices])
    return angles

def calc_angle_val(mol, box):
    angles = []
    for angle in mol:
        a = pbc(angle[0] - angle[1], box)
        a /= np.linalg.norm(a)
        b = pbc(angle[2] - angle[1], box)
        b /= np.linalg.norm(b)
        dot = np.dot(a,b)
        angles.append(dot)
    return np.array(angles)

def calculate_dihedrals(indices, positions, box):
    return np.array([calc_dihedral_val(link_to_pos(mol, positions), box) for mol in indices])

def calc_dihedral_val(mol, box):
    dihedrals = []
    for dihedral in mol:
        n1, b = get_normal_vec(dihedral[0:3], box)
        m = np.cross(n1, b)
        n2, _ = get_normal_vec(dihedral[1:], box)
        x = np.dot(n1, n2)
        y = np.dot(m, n2)
        dot = abs(np.arctan2(y, x))
        dihedrals.append(dot)
    return np.array(dihedrals)

def get_normal_vec(array, box):
    a = pbc(array[1]- array[0], box)
    a /= np.linalg.norm(a)
    b = pbc(array[2] - array[1], box)
    b /= np.linalg.norm(b)
    return np.cross(a,b), b
