import sys
sys.path.append("/Users/evanmiller/Projects/morphct/code")
from helperFunctions import loadPickle
from glob import glob
from numpy import load

def load_run(inputDir):

    File2Load = glob(inputDir + "/code/*.pickle")
    assert len(File2Load) == 1
    File2Load = File2Load [0]
    AAMorphologyDict, CGMorphologyDict, CGToAAIDMaster, parameterDict, chromophoreList = loadPickle(File2Load)

    KMCloader = glob(inputDir+"/KMC/KMCResults.pickle")
    assert len(KMCloader) == 1
    KMCloader = KMCloader[0]
    KMC = load(KMCloader)
    return AAMorphologyDict, CGMorphologyDict, CGToAAIDMaster, parameterDict, chromophoreList, KMC
