import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm as cm

data = pd.read_csv("data.csv")

corr_matrix = data.corr()

fig = plt.figure()
ax1 = fig.add_subplot(111)
cmap = cm.get_cmap('jet', 30)
cax = ax1.imshow(data.corr(), interpolation="nearest", cmap=cmap)
ax1.grid(True)
labels = list(data)
plt.title("DBP Correlations")
plt.xticks(np.arange(0,13), labels, rotation='vertical')
plt.yticks(np.arange(0,13), labels)
fig.colorbar(cax, ticks=[-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1.0])
plt.savefig("DBPcorr.pdf")
plt.close()


