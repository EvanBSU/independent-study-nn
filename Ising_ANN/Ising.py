"""
The purpose of this script is to train an Artificial Neural Net
to distinguish between high energy and ground-state Ising
states.
The Hamiltonian for this Ising system is defined by the 
sum of charges on bonds around the vertices.
"""

import numpy as np
import tensorflow as tf

def create_labels(vectors, state):
    if state == 'ground':
        labels = np.array([[1, 0] for i in vectors])
    if state == 'excited':
        labels = np.array([[0, 1] for i in vectors])
    return labels

def combine_vectors(vector1, vector2):
    return np.vstack((vector1, vector2))

def get_input_data():
    ground_state_vectors = np.load("ising_ground_state.npz.npy")
    #ground_state_vectors[ground_state_vectors < 0] = 0

    excited_state_vectors = np.load("ising_excited_state.npz.npy")
    #excited_state_vectors[excited_state_vectors < 0] = 0

    ground_state_labels = create_labels(ground_state_vectors, 'ground')
    excited_state_labels = create_labels(excited_state_vectors, 'excited')

    vectors = combine_vectors(ground_state_vectors, excited_state_vectors)
    labels = combine_vectors(ground_state_labels, excited_state_labels)

    p = np.random.permutation(len(vectors))

    return vectors[p], labels[p]

def set_up_model(input_length, output_length, x=None):
    if x == None:
        x = tf.placeholder(tf.float32, [None, input_length])
    W = tf.Variable(tf.zeros([input_length, output_length]))
    b = tf.Variable(tf.zeros([output_length]))

    y = tf.matmul(x, W)+b
    #y = tf.nn.sigmoid(tf.matmul(x, W)+b)

    y_ = tf.placeholder(tf.float32, [None, output_length])

    return x, W, b, y, y_

def select_training_testing_ratio(n_samples, ratio = 0.8):
    #Get indicies to cut input data into training and validation sets
    middle_point = n_samples//2 #Assume equal ground and excited state
    span = int(ratio*n_samples)//2
    start_point = middle_point-span
    end_point = middle_point+span
    return start_point, end_point

def run_NN(vectors, labels, N_hidden_nodes = 256):
    
    #Make sure inputs match
    assert len(vectors) == len(labels)

    #Get the dimensions of the inputs
    n_samples = len(vectors)
    vector_length = len(vectors[0])
    label_length = len(labels[0])

    #Set up the model
    print("Set up the model.")
    #x1, W1, b1, y, _ = set_up_model(vector_length, N_hidden_nodes)
    #x2, W2, b2, y2, y2_ = set_up_model(N_hidden_nodes, label_length, x=y)

    x = tf.placeholder(tf.float32, shape = [None, vector_length])
    y_ = tf.placeholder(tf.float32, shape = [None, label_length])

    #W1 = tf.Variable(tf.random_normal([vector_length, N_hidden_nodes]))
    #b1 = tf.Variable(tf.random_normal([N_hidden_nodes]))
    #y1 = tf.add(tf.matmul(x, W1), b1)

    W1 = tf.Variable(tf.random_normal([vector_length, label_length]))
    b1 = tf.Variable(tf.random_normal([label_length]))
    y1 = tf.add(tf.matmul(x, W1), b1)

    #W2 = tf.Variable(tf.random_normal([N_hidden_nodes, label_length]))
    #b2 = tf.Variable(tf.random_normal([label_length]))
    #y2 = tf.nn.sigmoid(tf.add(tf.matmul(y1, W2), b2))

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y1))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    #Start the session
    print("Initializing session.")
    session = tf.Session()
    init = tf.global_variables_initializer()
    session.run(init)

    #Get the training data
    start_point, end_point = select_training_testing_ratio(n_samples)
    train_vectors = vectors[start_point:end_point]
    train_labels = labels[start_point:end_point]

    #Begin the training
    print("Beginning training.")
    for training_iteration in range(100000):
        session.run(train_step, feed_dict={x:train_vectors, y_:train_labels})
    print("Finished training. Validating")

    #Get the evaluation data
    validation_vectors = np.vstack((vectors[:start_point], vectors[end_point:]))
    validation_labels = np.vstack((labels[:start_point], labels[end_point:]))

    #Evaluate
    comparison = tf.equal(tf.argmax(y1, dimension=1), tf.argmax(y_, dimension=1))
    correct = tf.reduce_mean(tf.cast(comparison, tf.float32))
    accuracy = session.run(correct, feed_dict = {x:validation_vectors, y_:validation_labels})
    print("The ANN had an {:.2f}% accuracy".format(accuracy*100))
    
if __name__ == "__main__":
    vectors, labels = get_input_data()
    run_NN(vectors, labels)
