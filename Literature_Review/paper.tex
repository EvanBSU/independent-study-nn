% typeset for review
% \documentclass[journal = jctcce]
% match the journal format as closely as possible
\documentclass[journal = jacs, manuscript = article, layout = onecolumn]{achemso}

\usepackage{graphicx}
\graphicspath{{images/}}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{amsmath}
%\usepackage{lineno}
%\biboptions{square, comma, numbers, sort&compress}

% for hyperlinks
\usepackage[usenames,dvipsnames]{color}
\definecolor{darkblue}{rgb}{0,0.08,0.45}
\usepackage[bookmarksopen=true,bookmarksopenlevel=2,colorlinks=true,linkcolor=darkblue,anchorcolor=darkblue,citecolor=darkblue,urlcolor=darkblue,pdfstartview=FitH]{hyperref}

% commands for formatting in algorithms
%\usepackage{algorithm}
%\usepackage{algpseudocode}
%\newcommand{\algorithmname}{Algorithm}
\newcommand{\var}[1]{\mathtt{#1}}
\newcommand{\func}[1]{\mathbf{#1}}
\newcommand{\set}[2]{#1 \leftarrow #2}
\newcommand{\load}[2]{#1 \Leftarrow #2}
\newcommand{\store}[2]{#1 \Rightarrow #2}

\newenvironment{DIFnomarkup}{}{}
\newcommand{\todo}[1]{\textcolor{red}{\textit{#1} -- TODO}}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{<author name>}
%% \address[label1]{<address>}
%% \address[label2]{<address>}
\author{Evan Miller}
\email{evanmiller326@boisestate.edu}

%% Title, authors and addresses
\title{Artificial Neural Networks and Their Application in Materials Science and Engineering}

%\keywords{molecular dynamics, organic photovoltaics, coarse-graining}

\begin{document}

Artificial Neural Networks (ANNs) are inspired by the neural pathways in brains: the firing of connected neurons corresponds to patterns between an input and an output.
The simplest ANNs consist of only an input layer connected to an output layer \cite{Bishop1995}.
In more complex ANNs many hidden, intermediate layers are present and allow more complicated functions (and more complicated relationships) to be identified by the ANN.
However, regardless of complexity, these ANNs operate as a sequence of matrix multiplications that correlate the input vector $\vec{x}$ to an output $y$, where $y$ can be a vector or scalar.
The process of developing the matrices that will link the inputs and outputs is called ``learning'' and comes in two forms: supervised and unsupervised.
For supervised learning, inputs with known outputs are provided to the ANN, and the matrix $M$ elements linking the inputs and outputs are tuned until $\vec{x} * M$ gives the correct $y$ through a process called back propagation \cite{Werbos1974}.
For unsupervised learning, no targeted results are supplied to the ANN, rather the ANN is left to extract relationships within the data on its own.
In this report, we focus primarily on supervised learning for ANNs, and the methods that enable creation of the matrix to link inputs and outputs.

ANNs were first developed in the 1960s by Rosenblatt\cite{Rosenblatt1958} and by Widrow and Lehr \cite{Widrow1990}. 
For these systems, the activation function $a(\cdot)$, which is the equation that determines if a neuron will fire, was based on a set of threshold functions.
In a threshold function, the neuron will produce a binary output $y^{n}$ based on $\vec{x}$.
For example: if the sum of the inputs into the neuron is a negative number, then $y^{n}=-1$, and, if the sum of the inputs is a positive number or zero, then $y^{n}=+1$.
To train the neuron, tunable weights $w$ and biases $b$ are applied to every $x_i$ into the neuron.
Putting it mathematically:
\begin{equation}\label{sims:eq:Forster}
y^{n} = \begin{cases} 
1 & \quad \text{if } \sum_i (w_i \times x_i + b_i) \geq 0,\\
-1 & \quad \text{if } \sum_i (w_i \times x_i + b_i) < 0.\\
 \end{cases}
\end{equation}
These neuron types do have weaknesses in that they are unable to solve linearly inseparable problems such as the Exclusive OR (XOR) problems \cite{Minsky1969}.
Additionally, because of the binary nature of the threshold function, small changes in $w_i$ or $b_i$ could incorrectly change the behavior of a neuron. 
Even one neuron firing incorrectly could cause a large effect on subsequent neurons and lead to instabilities and errors in training the ANNs.
To overcome this issue, softer $a(\cdot)$ have also been implemented that have a continuous derivative.
Examples of softer $a(\cdot)$ include: linear, sigmoid and hyperbolic tangent functions.
Sigmoid and hyperbolic tangent functions approximate a step function and differ only in a linear transformation (however hyperbolic tangent functions are preferable as they normally converge faster) \cite{Bishop1995, Artrith2016}.

The differentiable nature of $a(\cdot)$ is often important in training the ANN to recognize a pattern in $\vec{x}$.
When the ANN has been given $\vec{x}$ with a known $y$, $w_i$ and $b_i$ can be evaluated to determine the error $E$ that these $w_i$ and $b_i$ produce:
\begin{equation}
E = \sum_{i}^{N} E_i(w_i, b_i),
\end{equation}
where $E_i$ is the error on neuron $i$.
There are multiple ways to calculate $E(w,b)$, but perhaps the easiest conceptually is the sum-of-squares error:
\begin{equation}
E = \frac{1}{2} \sum_i^N (y^n -t^n)^2,
\end{equation}
where $y^n$ is the output of neuron $n_i$ (not necessarily binary) and $t^n$ is the output that would produce the correct $y$.
The error will reach a minimum when:
\begin{equation}
\frac{\delta E}{\delta w} = 0.
\end{equation}
This minimum can be achieved when $\Delta w$ is back propagated through the ANN by gradient descent \cite{Rumel1986}:
\begin{equation}
\Delta w = - \eta \delta_j x_i,
\end{equation}
in which $\eta$ is the learning rate and $\delta_j$ describes the ``errors'' \cite{Widrow1960}.
The learning rate $\eta$ acts as the step size on the error surface and is chosen by the user.
Care must be taken when choosing $\eta$ as larger values will lead to faster convergence to $E_{min}$, but too large a value will overshoot $E_{min}$ and may prevent convergence \cite{Goyal2017}.
Other methods of back propagation are also available, such as the Jacobian and Hessian matrices, which are matrices containing the first and second derivatives respectfully. 
The Hessian matrix can also be used to quickly retrain a network with if a small change in training data occurs\cite{Bishop1991}.

Before we leave the topic of error functions, we should consider another flavor of error function.
ANNs are built to accomplish two types of pattern recognition: regression and classification.
In regression, the ANN is trying to map $\vec{x}$ to a scalar value $y$.
However, for classification problems, the answer will be a distinct member of a class, and as such, $y$ is often a vector where each element in the vector describes the membership to a class.
For these types of problems, the ``cross entropy'', built on the log likelihood, is more appropriate and faster than to describe the error in $w$ and $b$\cite{Baum1988, Solla1988}.
In classification problems the ANN is calculating a probability distribution $q(x)$ that the $\vec{x}$ belongs to a certain class.
For this process, there is a true probability distribution $p(x)$ describing membership to this class (which is supplied from the training data).
The measure to which the $q(x)$ and $p(x)$ agree can be calculated with:
\begin{equation}
\varepsilon = - \int p(x) \ln(q(x)) dx,
\end{equation}
and is termed the cross entropy between the distributions $q(x)$ and $p(x)$. 
This entropy will be minimized when the model probability equals the true probability: $q(x)=p(x)$, therefore, in training the ANN we tune the $w$ and $b$ to achieve the correct probability distribution.
In training these classification problems, it is often also beneficial to apply a softmax activation function, so that one value in the output is one and the others zero to identify the class to which $\vec{x}$ belongs\cite{Bridle1990}. 

Up until now, we have only considered tuning $w$ and $b$ as ways to minimize the error in the ANN, however, it is also possible to grow the number of neurons and layers to match patterns more accurately.
When a neuron incorrectly fires, we could consider adding two additional neurons between this neuron and the next layer. 
Each of these child neurons are then trained to correct for one inaccuracy in the parent: one for when it fires incorrectly and one where it incorrectly fails to fire.
We are thereby able to increase the accuracy of the ANN without affecting the global $w$ and $b$ parameters.
Because, these $w$ and $b$ are often unknown to the user, it is necessary to utilize a method that doesn't need human intervention to add neurons.
This automation can be accomplished through various algorithms including: the pocket algorithm-designed to deal with datasets with that are not linearly separable \cite{Gallant1986}, the tiling algorithm that adds consecutively smaller layers \cite{Mezard1989}, and upstart algorithms, which are used in our example above\cite{Frean1990}. 

Adding more neurons or hidden layers in the ANN may increase the accuracy with respect to the training data, however, complexity can be also be less favorable overall.
The worth of these ANNs is not in the ability to represent the training data, rather their predictive ability on new data.
Therefore, the accuracy of the ANN must be balanced with its generalization, and superfluous complexity will decrease generalization and increase training times.
One methodology to limit the complexity of the ANN is to add a complexity penalty or regularization term $\Omega$ to the error function:
\begin{equation}
\widetilde{E} = E + \nu \Omega
\end{equation}
where $\nu$ is a tunable parameter of how strictly the complexity penalty is applied.
One simple way of choosing an $\Omega$ is purely by the sum-of-squares of adaptive parameters in the ANN:
\begin{equation}
\Omega = \frac{1}{2} \sum_i w_i^2,
\end{equation}
which is called ``weight decay'', and has shown significant improvements in the generalization of ANNs \cite{Hinton1987}.
However, this form also has a limitation in that it will bias towards many small weights.
A way to overcome this is to use a modified decay term by adding an additional scaling parameter $\hat{w}$ \cite{Lang1990}:
\begin{equation}
\widetilde{E} = E + \nu  \sum_i \frac{w_i^2}{\hat{w}^2+w_i^2},
\end{equation}
which will bias towards fewer, higher $w$ and can be conducted during the training process, but requires selecting $\hat{w}$.
Another form of regulator is that of Tikhonov, which is designed for one input parameter $x$ and one output parameter $y$ and has been used in the context of vision systems \cite{Golub1999, Poggio1985}.

Other pruning algorithms are iterative with the training process. 
To accomplish this we need to calculate the saliency of the weights, which is a measure of how much a particular $w_i$ affects the $E$.
A brute-force method for this is to iterate through all the $w_i$, setting them to zero, then calculating the associated $E$ with this change.
However, this method is computationally expensive and unfavorable.
It is more computationally favorable to use the Hessian matrix $H$ with two forms termed: ``optimal brain damage'' and ``optimal brain surgery''.
In optimal brain damage, the non-diagonal terms in $H$ are discarded, which can be a poor assumption, but is computationally more efficient, and has given acceptable results \cite{Cun1990}.
In the optimal brain surgery, this approximation for $H$ is not made, and provides more accurate results than the optimal brain damage algorithm \cite{Hassibi1993}.
In addition to removing weights, it is also possible to remove entire nodes from the ANN, by using a similar method to calculate the saliency of a node then removing the high saliency nodes \cite{Bishop1995}.

If we are not wanting to reduce the complexity of the model, we can still increase the generalization of our ANN by training it with noise or terminating the training process early.
When training with noise, the noise will prevent over-fitting of the values $\vec{x}$ to $y$.
In early termination, the training process is stopped when the training accuracy reaches a certain level so that the $w$ are frozen, and more fitting cannot occur.
Regardless which method is used to increase generality, a good practice is to calculate the actual generality of the ANN by calculating the error on a set of data external to the training set.

Even with complex models, it can still be difficult for the ANN to determine relationships between $\vec{x}$ and $y$, which brings us to another important factor: preparing $\vec{x}$ in a process termed ``feature extraction'' \cite{Bishop1995}.
ANNs, like other forms of machine learning, are subject to the ``curse of dimensionality'', which is the postulate that the amount of training required increases exponentially with the dimensionality of the problem \cite{Bellman1961}.
Therefore, it is often necessary to utilize prior knowledge to reduce the dimensionality of $\vec{x}$.
As an example of this, I refer to our previous work in determining if equilibrium is reached \cite{Leibowitz2017a}.
We know that reaching equilibrium is characterised by the relative decrease in the potential energy over time, not the absolute magnitude of the data.
Therefore, when we subtract out the mean value of $\vec{x}$ from $\vec{x}$, we remove the magnitude-of-energy dimension from the pattern, which resulted in a more accurate ANN.
There are also automated ways to overcome the challenge of dimensionality, such as convolutional or multilayer ANNs in which the initial layers of the ANN are used to filter the important dimensions of the input data \cite{Bishop1995}.
Furthermore, there are algorithms to identify the important dimensions such as unsupervised training and sequential searching techniques. 
In sequential searching techniques there is sequential backward elimination in which all dimensions are included at the beginning of the training, then dimensions are iteratively removed.
Converse to this process, we can use sequential forward selection in which only one dimension is included initially, and additional dimensions are added later.
However, these sequential searching techniques can be computationally expensive and do not guarantee that the optimal subset of dimensions will be found\cite{Bishop1995}.

Now that we have covered many of the primary topics in creating an ANN, we turn to their application in Materials Science and Engineering (MSE).
Recently, there has been a lot if interest in applying the ANNs, in my opinion due to the computational speed ups that they receive on GPUs \cite{Oh2004}.
There have been multiple reviews of ANNs in MSE, including: Ihom who talks about applications and issues with the use of ANNs in MSE \cite{Offiong2015}, Singh who essentially gives a list of ANN applications in MSE \cite{Singh2016a}, and the works of Bhadeshia\cite{McCormick2010, Bhadeshia1999}.
There has also been a paper by Sha in which he conducts a scathing review of the errors researchers have made in the application ANNs, which elucidates several mistakes to avoid \cite{Sha2007}.
Other than reviews, some common topics that ANNs have been applied to have been: learning potential energy surfaces and/or developing new force-fields for simulations \cite{Lorenz2004,Artrith2016}, drug discovery \cite{El-Telbany2014, Ramsundar2015}, quantifying or predicting structure \cite{Fayos2005, Carrasquilla2016,Reinhart2017}, and nuclear reactors \cite{Gomes2015, Messai2015}.

We now explore some applications of ANNs in more detail to discuss accomplishments and shortcomings of ANN studies found in literature.
For instance, one flawed application of an ANN to a materials science question is in the work of Perea et al \cite{Perea2016}.
In this work, they are trying to train an ANN to determine the Hildebrad and Hansen solubility parameters using $\sigma$ values, which are a measure of the surface charge distributions from quantum chemical calculations, for fullerene derivatives.
This work is an extension of an earlier paper, by Jarvas where they train an ANN with 113 different molecules\cite{Jarvas2011}.
The flaw in the Perea paper is the number of nodes used in the hidden layer.
In the original work, sets of either 5 or 12 $\sigma$ parameters are used, whereas the maximum used in the more recent investigation is five.
In the hidden layer of the ANN, they then have 13 nodes.
With so few inputs to hidden nodes, it is likely that they are over fitting their data to the training set.
Another general criticism of the child paper, is the lack of explanation of how the fullerenes are used in the ANN process. 
I am unaware if any were used in the training or if the parent ANN was applied to the fullerene species.

Another flawed application of an ANN is that done by Altinkok and Koker \cite{Altinkok2006}.
In their work, the authors are trying to link the size of Al$_2$O$_3$/SiC particles to the bending strength, hardness and porosity of the aluminum matrix.
To train the ANN, only particle size are given as an input but all three properties (strength, hardness and porosity) are taken as outputs, with 5 nodes in the hidden layer.
One issue I see with their work is rather than having three ANNs that each did one job well (or better) they tried to train the ANN to simultaneously determine the relationships with three parameters.
In addition to this, they only have 15 total training samples and 240,000 training iterations which means they are strongly training their net to few data points.
Furthermore, they claim their ANN a success because the mean squared error that is calculated in the training step is quickly reduced to zero, but with so few input data points and so many training iterations, they are likely over-fitting their data heavily, and they do not quantify the error with data that is external to the training set.

Turning now to better applications, the first paper that I thought was good was \textit{Convolutional Networks on Graphs for Learning Molecular Fingerprints} by Duvenaud et al \cite{Duvenaud2015}.
In this paper, they create molecular fingerprints with a convolutional ANN and it enables increased predictability and interpretability of molecular structures.
Although in this work Duvenaud is looking at molecules, I can imagine that this methodology could also be describe local morphology based on cluster fingerprints (like the cluster analysis Long and Ferguson did \cite{Reinhart2017})..
This may be an interesting experiment to do.

The second paper that I liked was \textit{On-line fault detection of a fuel rod temperature measurement sensor in a nuclear reactor core using ANN} by Massai et al\cite{Messai2015}.
In this work, the authors train an ANN to predict the temperature of a nuclear reactor so that if the actual temperature deviates too far it will indicate that there is a fault inside 
I do not think this paper is incredibly ground breaking in what they were able to accomplish, but the discussions of the authors' methods and justification were particular enlightening.
Firstly, they go into depth about how they normalized the data by eliminating noise by using a discrete wave transformation.
Secondly, they are able to use real time data (eight hours worth and a reading occurring every 4 seconds) to train the ANN.
Finally, they discuss the various activation functions and ANN architectures they tested (13 algorithms with various layers and nodes) along with the results they produced.
Overall, as someone trying to better understand how to apply ANNs in my work, this was a thorough example of the machine learning methodology.

The final paper that I thought was particularly impressive was that of Ramsundar et al. in which they are applying an ANN for drug discovery \cite{Ramsundar2015}.
The dataset size for this work was particularly impressive: 40 million measurements across 200 biological targets.
Likely because their dataset is so big, they don't perform any preprocessing to remove potential artifacts.
Similar to Duvenaud, they also use the RDKit to describe molecular species.
The authors also use various forms machine-learning and find that a pyramidal (2,000 first layer, 1,000 second layers) multi-task ANN provides the most accurate results.
Another finding is that their multi-task ANNs are less affected by duplicate measurements in their inputs.
It is nice to see how increased dataset size really does correlate to overcoming the curse of dimensionality in the ANN.

\bibliography{library}

\end{document}
