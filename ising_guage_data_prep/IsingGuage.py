import numpy as np
import matplotlib.pyplot as plt
import sys
from multiprocessing import Pool

def pbc(pair):
    """
    From a pair of coordinates ([x,y] list) apply the 
    periodic boundary conditions.
    """
    newpair = np.copy(pair)
    if newpair[0] >= 31:
        newpair[0] -= 32 
    if newpair[1] >= 15:
        newpair[1] -= 16 
    return newpair[0], newpair[1]

def calc_energy(array):
    """
    Calculate the sum of energies from each plaquette in the array.
    Requires the 16x16x2 array of spin states.
    """
    E = 0
    for index in indices:
    #Only look at plaquettes on odd rows to stop double counting.
        if index[0] % 2 == 1:
            p1, p2 = mult_neighbors(index[0], index[1], array, sides = True)
            E+= p1
    return E

def trialMove(array, T):
    """
    Use pseudo-random numbers to select a spin within the model.
    """
    
    x1, y1 = np.random.randint(0, 32), np.random.randint(0, 16)
    array, dE = DeltaE(x1, y1, array, T)
    
    return array, dE

def DeltaE(x1, y1, array, T):
    """
    Flip a spin and calculate the difference in energy with flip
    If the energy is lowered, return array with the flip otherwise
    return the original array.
    """
    newarray = np.copy(array)
    newarray[x1,y1] = newarray[x1, y1]*-1
    #Because we have a 16x16x2 lattice for 16x16 plaquettes we need
    #to get spins to the sides or above and below based on the spin flipped
    if x1 %2 == 1:
        #Need left and right
        p1new, p2new = mult_neighbors(x1,y1,newarray,sides=True)
        p1old, p2old = mult_neighbors(x1,y1,array,sides=True)
    else:
        #Need above and below
        p1new, p2new = mult_neighbors(x1,y1,newarray,vertical=True)
        p1old, p2old = mult_neighbors(x1,y1,array,vertical=True)
    dE = (p1new + p2new) - (p1old + p2old) 
    if  dE <= 0 or T != 0:
        del array
        return newarray, dE
    else: 
        del newarray
        return array, 0

def mult_neighbors(x, y, array, sides = False, vertical=False):
    """
    Multiply the spins around two plaquettes when given the coords
    for a shared spin.
    """
    if sides:
        p1 = array[pbc([x,y])]*array[pbc([x,y-1])]*array[pbc([x+1,y-1])]*array[pbc([x-1,y-1])]
        p2 = array[x,y]*array[pbc([x,y+1])]*array[pbc([x+1,y])]*array[pbc([x-1,y])]
    if vertical:
        p1 = array[x,y]*array[pbc([x-1,y])]*array[pbc([x-2,y-1])]*array[pbc([x-1,y+1])]
        p2 = array[x,y]*array[pbc([x+1,y])]*array[pbc([x+2,y])]*array[pbc([x+1,y+1])]
    return p1, p2

def run(array, T=0):
    newarray = np.copy(array)
    array, dE = trialMove(newarray, T)
    return array, dE

def create_Ising_lattice(iteration=None):
    """
    Generate a random Ising lattice
    """
    #np.random.seed() needed with multiprocessing
    np.random.seed()

    negative = np.ones(8 * 32)*-1
    positive = np.ones(8 * 32)
    array = np.hstack((negative, positive))
    np.random.shuffle(array)
    array = array.reshape((32, 16))
    return array

def visualize_lattice(array):
    """
    Visualize the Ising lattice.
    Function does some somersaults to convert the shape of
    the array into a more human readable lattice.
    """
    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(111)
    color_dict = {-1:'k',1:'y'}
    indices = np.array([[j, i] for i in range(0,16) for j in range(0,32)])
    ax.clear()
    colors = [color_dict[array[j, i]] for i in range(0,16) for j in range(0,32)]
    ax.scatter(indices[::2,1], -indices[::2,0], c = colors[::2])
    ax.scatter(indices[1::2,1]+0.5, -indices[1::2,0], c = colors[1::2])
    plt.axis('off')
    plt.show()

def create_big_array(iterations):
    """
    'Efficiently' do a high number of iterations
    of the run_single and convert it into one big
    array.
    """
    arrays = Pool().map(run_single, np.arange(iterations))
    arrays = np.array([np.array(array).reshape((1,512)) for array in arrays])
    return arrays

def build_distributions():
    """
    For troubleshooting: Plot the distribution of energies from
    the ground and excited states to ensure ANN is getting correct
    data.
    """
    states = {'excited':[],'ground':[]}
    for state in ['excited', 'ground']:
        big_array = np.load('ising_{}_state.npz.npy'.format(state))
        np.random.shuffle(big_array)
        for i in range(2000):
            array = big_array[i]
            array = array.reshape((32, 16))
            states[state].append(calc_energy(array))
    return states

def plot_distribution(states):
    """
    Does the actual plotting for build_distributions()
    """
    plt.close()
    for state in states:
        plt.hist(states[state], bins = 50)
    plt.show()

def run_single(return_dE = False, iteration = None):
    """
    Create and equilibrate an Ising lattice in
    the Guage Model.
    1e4 timesteps seems to be sufficient to reach eq.
    """
    array = create_Ising_lattice()
    for _ in range(int(1e3)):
        #Run this many equilibration steps.
        array, dE = run(array, T = 1)#dE was for troubleshooting to see system equilibrate
    #dE creates issues with multiprocessing so not returning unless need more troubleshooting.
    return array

def remove_array_layer():
    data = np.load("guage_ground.npy")
    data_saved = np.array([data[i][0] for i in range(len(data))])
    np.save("guage_ground", data_saved)

if __name__ == "__main__":
    indices = np.array([[i,j] for i in range(0,32) for j in range(0,16)])
    remove_array_layer()

    #arrays = create_big_array(int(1e4))
    #np.save("guage_excited", arrays)
