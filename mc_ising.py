import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation

def build_mappers(size):

    west = np.arange(0, size*size, 1)*2

    south = np.arange(0, size*size, 1)*2+1

    north = np.arange(0, size*size, 1)*2-1
    north = north.reshape(size, size)
    north[:,0] += 2*size
    north = north.flatten()

    east = np.arange(0, size*size)*2+2*size
    east = east.reshape(size,size)
    east[-1,:] -= size*size*2
    east = east.flatten()

    bonds_to_vertices_mapper = {}
    vertices_to_bonds_mapper = {}

    for bonds in zip(np.arange(size*size), north, east, south, west):
        vertices_to_bonds_mapper[bonds[0]] = list(bonds[1:])

    for i in range(size*size*2):
        bonds_to_vertices_mapper[i] = []

    for key, pair in vertices_to_bonds_mapper.items():
        for index in pair:
            bonds_to_vertices_mapper[index].append(int(key))

    return vertices_to_bonds_mapper, bonds_to_vertices_mapper

class Ising:
    def __init__(self, size=16, seed=1234, T = 10., model = 'square-ice'):
        np.random.seed(seed)
        self.H = []
        self.T = T
        self.model = model
        self.accept = 0
        self.steps = 0
        self.columns = size
        self.rows = size*2
        self.bonds = np.random.random_integers(low=0,high=1,size=(self.rows,self.columns))
        self.bonds = self.bonds.flatten()
        self.bonds[self.bonds==0]=-1

        self.nbonds = self.columns*self.rows

        self.vertices = np.zeros((size,size),dtype='int64').flatten()

    def run(self, steps = 1):

        for i in range(int(steps)):
            self.trial_move()


    def trial_move(self):
        self.trial = np.copy(self.bonds)
        i = np.random.randint(self.nbonds)
        self.trial[i] *= -1

        v1, v2 = self.bonds_to_vertices_mapper[i]
        e_init = self.vertices[v1] + self.vertices[v2]

        if self.model == 'square-ice':
            e_v1_final = self.trial[self.vertices_to_bonds_mapper[v1]].sum()**2
            e_v2_final = self.trial[self.vertices_to_bonds_mapper[v2]].sum()**2
        if self.model == 'gauge':
            e_v1_final = -1*np.prod(self.trial[self.vertices_to_bonds_mapper[v1]])
            e_v2_final = -1*np.prod(self.trial[self.vertices_to_bonds_mapper[v2]])

        e_final = e_v1_final + e_v2_final

        dE = e_final - e_init

        if np.random.rand() < np.exp(-dE/self.T):
            self.bonds = self.trial
            self.vertices[v1] = e_v1_final
            self.vertices[v2] = e_v2_final
            self.accept += 1

        self.steps += 1
        energy_total = self.vertices.sum()
        self.H.append(energy_total)

    def get_mappers(self, vertices_to_bonds_mapper, bonds_to_vertices_mapper):
        self.vertices_to_bonds_mapper = vertices_to_bonds_mapper
        self.bonds_to_vertices_mapper = bonds_to_vertices_mapper
        
    def apply_bonds(self):
        for i, val in enumerate(self.vertices):
            if self.model == 'square-ice':
                self.vertices[i] = self.bonds[self.vertices_to_bonds_mapper[i]].sum()**2
            if self.model == 'gauge':
                self.vertices[i] = -1*np.prod(self.bonds[self.vertices_to_bonds_mapper[i]])

    def plot_system(self, title, filename = "indexing.png"):
        plt.close()

        color_dict = {-1:'k',1:'none'}
        if self.model == 'square-ice':
            energy_color_dict = {0:'g',4:'y',16:'r'}
            alphas_dict = {0:0.1,4:0.5,16:1.0}
            vmin = 0
            vmax = 16
        if self.model == 'gauge':
            energy_color_dict = {-1:'g',1:'b'}
            alphas_dict = {-1:0.1,1:1.0}
            vmin = -1
            vmax = 1
        #Reshape the array for the plot
        spins = np.copy(self.bonds)
        #Assign the fill based off of spin.
        indices = np.array([[i, j] for j in range(0,self.columns) for i in range(0,self.rows)])
        vertex_indices = np.array([[i*2, j+0.5] for j in range(0,self.columns) for i in range(0,self.rows//2)])

        #colors = [color_dict[spins[j, i]] for i in range(0,self.columns) for j in range(0,self.rows)]
        colors = [color_dict[i] for i in spins]
        energy_colors = [energy_color_dict[i] for i in self.vertices]
        alphas = [alphas_dict[i] for i in self.vertices]

        #rgba_colors = np.zeros((size*size,4))
        #rgba_colors[:,0] = 1.0
        #rgba_colors[:, 3] = np.array(alphas)

        indice_labels = np.array([i for i in range(0,self.columns*self.rows)])
        vertex_labels = np.array([i for i in range(0,self.columns*self.columns)])

        #for lab in zip(indices[::2,1], -indices[::2,0], indice_labels[::2]):
        #    plt.text(lab[0]+0.05, lab[1], s=lab[2], fontsize=12)

        #for lab in zip(indices[1::2,1], -indices[1::2,0], indice_labels[1::2]):
        #    plt.text(lab[0]+0.55, lab[1], s=lab[2], fontsize=12)

        #for lab in zip(vertex_indices[:,1], -vertex_indices[:,0], vertex_labels):
        #    plt.text(lab[0]+0.05, lab[1], s=lab[2], fontsize=12, color='red')

        plt.scatter(vertex_indices[:,1], -vertex_indices[:,0], c = energy_colors, alpha = 0.5, vmin =vmin, vmax=vmax )
        #plt.scatter(vertex_indices[:,1], -vertex_indices[:,0], c = rgba_colors)
        plt.scatter(indices[::2,1], -indices[::2,0], facecolor = colors[::2], edgecolor = 'k')
        plt.scatter(indices[1::2,1]+0.5, -indices[1::2,0], facecolor = colors[1::2], edgecolor = 'k') 
        plt.axis('off')
        plt.title(title)

        plt.savefig(filename)

def plot_energies(e_dict):
    plt.close()
    for key, pair in e_dict.items():
        plt.plot(np.arange(len(pair)), pair, label = "{}".format(key))
        plt.xlim([0, len(pair)*1.3])
        plt.xlabel('Trial')
        plt.ylabel('Energy')
        plt.title("E v step")
        plt.legend(fontsize=12)
        plt.savefig("E-over-time.png")

def plot_e_v_T(e_dict):
    from operator import itemgetter
    plt.close()
    e_list = []
    for key, pair in e_dict.items():
        E_final = np.mean(pair[(len(pair)//2):])
        E_std = np.std(pair[len(pair)//2:])/np.sqrt(len(pair)/2)
        e_list.append([key, E_final, E_std])
    e_list = sorted(e_list, key=itemgetter(0))
    e_list = np.array(e_list)
    plt.errorbar(e_list[:,0], e_list[:,1], yerr=e_list[:,1])
    plt.xlabel("T")
    plt.ylabel("E")
    plt.savefig("E-v-T.png")

if __name__ == "__main__":
    size = 16
    vertices_to_bonds_mapper, bonds_to_vertices_mapper = build_mappers(size)
    kTs = [0.1, 0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 25.0]
    e_dict = {}
    for T in kTs:
        ising = Ising(size = size, seed = np.random.randint(1000000), T=T, model='square-ice')
        ising.get_mappers(vertices_to_bonds_mapper, bonds_to_vertices_mapper)
        ising.apply_bonds()
        ising.run(2e5)

        Hfinal = ising.vertices.sum()
        title = r"kT={:.1f}, H$_f$={}".format(T, Hfinal)
        filename = "lattice-kT-{:.1f}.png".format(T)

        ising.plot_system(title=title, filename=filename)

        print(ising.accept/ising.steps)
        e_dict[T] = ising.H
    plot_energies(e_dict)
    plot_e_v_T(e_dict)
