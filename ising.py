import numpy as np
import sys
import matplotlib.pyplot as plt

class Ising:
    def __init__(self,size = 32,seed=1234,T=16.):
        np.random.seed(seed)
        self.H = []
        self.T=T
        self.accept = 0
        self.steps = 0
        self.size2 = size*size
        self.nbonds = 2*size*size
        self.bonds = np.random.random_integers(low=0,high=1,size=(size,2*size)).flatten()
        self.bonds[self.bonds==0]=-1
        self.vertices = np.zeros((size,size),dtype='int64').flatten()
        self.north = np.array([size*i*2+j for i in range(size) for j in range(size)])
        self.east = np.array([size*i*2+j+size for i in range(size) for j in range(size)])
        self.south = np.array([size*i*2+j+2*size for i in range(size) for j in range(size)]).reshape(size,size)
        self.south[-1]-=self.nbonds
        self.south=self.south.flatten()
        self.west = np.array([size*i*2+j+size-1 for i in range(size) for j in range(size)]).reshape(size,size)
        self.west[:,0]+=size
        self.west=self.west.flatten()
        self.trial = np.copy(self.bonds)

    def calcH(self,lattice):
        self.vertices.fill(0)
        self.vertices += lattice[self.north]
        self.vertices += lattice[self.south] 
        self.vertices += lattice[self.east] 
        self.vertices += lattice[self.west] 
        return (self.vertices*self.vertices).sum()

    def trialMove(self):
        self.steps+=1
        i = np.random.randint(self.nbonds)
        self.trial[i]*=-1
        tH = self.calcH(self.trial)
        bH = self.calcH(self.bonds) #can optimize
        delta = tH-bH
        if np.random.rand() < np.exp(delta/self.T):
            self.bonds[i]*=-1
            self.accept+=1
            self.H.append(tH/self.size2)
        else:
            self.H.append(bH/self.size2)
            self.trial[i]*=-1

    def run(self,steps=1):
        for i in range(steps):
            self.trialMove()

size=16
for t in range(2):
    i = Ising(size=size,T=1,seed=np.random.randint(1000000))
    i.run(steps=int(2e5))
    print(i.bonds.reshape(size,2*size)+1)
    print(i.accept/i.steps)
    print(np.average(i.H),np.std(i.H))
    plt.plot(i.H)
    plt.show()

